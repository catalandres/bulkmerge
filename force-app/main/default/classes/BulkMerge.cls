public without sharing class BulkMerge {

	@testVisible
	private static final String ERROR_INVALID_PAYLOAD = 'The BulkMerge payload is not valid.';

	public class BulkMergeException extends Exception {}

	public static void run(Map<SObject, List<SObject>> payload) {
		if (!validate(payload)) {
			throw new BulkMergeException(ERROR_INVALID_PAYLOAD);
		}
		System.enqueueJob(new BulkMergeJob(payload));
	}

	public static Boolean validate(Map<SObject, List<SObject>> payload) {
		for (SObject master: payload.keySet()) {
			if (!(master instanceOf Lead)
				&& !(master instanceOf Account) 
				&& !(master instanceOf Contact)
			) {
				return false;
			}
			for (SObject duplicate: payload.get(master)) {
				if (master.getSObjectType() != duplicate.getSObjectType()) {
					return false;
				}
			}
		}
		return true;
	}

	public class BulkMergeJob implements Queueable {

		private final Map<SObject, List<SObject>> payload;
		private Integer dmlCapacity;

		public BulkMergeJob(Map<SObject, List<SObject>> payload) {
			this.payload = payload;
		}

		public void execute(QueueableContext context) {
			this.dmlCapacity = Limits.getLimitDMLStatements() - Limits.getDMLStatements();

			if (dmlCapacity == 0) {
				return;
			}

			for (SObject master: payload.keySet()) {
				List<SObject> duplicates = payload.remove(master);
				if (duplicates == null) { continue; }
				while (duplicates.size() > 0 && dmlCapacity > 0) {
					List<SObject> duplicatesToMerge = new List<SObject>();
					duplicatesToMerge.add(duplicates.remove(0));
					if (duplicates.size() > 0) {
						duplicatesToMerge.add(duplicates.remove(0));
					} 
					Database.merge(master, duplicatesToMerge);
					dmlCapacity--;
				}
				if (dmlCapacity == 0) {
					continueJob(master, duplicates);
					break;
				}
			}
		}

		private void continueJob(SObject master, List<SObject> duplicates) {
			if (duplicates.size() > 0) {
				payload.put(master, duplicates);
			}
			if (payload.size() > 0) {
				System.enqueueJob(new BulkMergeJob(payload));
			}
		}
	}
}
